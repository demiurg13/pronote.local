<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'number', 'status', 'master', 'notice', 'closed_at'
    ];

    /**
     * [scopeStatus description]
     * @param  \Illuminate\Database\Eloquent\Builder $query запрос
     * @param  string $status Статус заказа
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * Поиск по заказам для персонала
     */
    public function scopeSearchAdmin($query, $search)
    {
        return $query->where('number', 'like', '%'.$search.'%')
                  ->orWhere('master', 'like', '%'.$search.'%')
                  ->orWhere('notice', 'like', '%'.$search.'%');
    }
    /**
     * Поиск по заказам для пользователй
     */
    public function scopeSearchUser($query, $search)
    {
        return $query->where('number', 'like', $search);
    }
}
