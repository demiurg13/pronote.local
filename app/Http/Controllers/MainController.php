<?php

namespace App\Http\Controllers;

use App\Information;
use App\Office;
use App\Order;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(Request $request)
    {

        $search = $request->input('search');
        if (isset($search)) {
            $order = Order::searchUser($search)->first();
        } else {
            $order = Order::searchUser('')->first();
        }

        return view('index', [
            'information' => Information::where('name', 'pronote')->first(),
            'offices' => Office::all(),
            'search' => $search,
            'order' => $order,
        ]);
    }
}
