<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    // Панель администратора
    public function dashboard()
    {
        return view('admin.dashboard', [
            'completedOrders' => Order::where('status', 'Ремонт завершен')->orderBy('number')->get(),
            'waitOrders' => Order::where('status', 'Требует согласования')->orderBy('number')->get(),
        ]);
    }
}
