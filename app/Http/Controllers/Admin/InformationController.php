<?php

namespace App\Http\Controllers\Admin;

use App\Information;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.informations.index', [
            'information' => Information::where('name', 'pronote')->first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        return view('admin.informations.edit', [
            'information' => $information
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        $validator = $request->validate([
            'company' => 'required|string|max:255',
            'inn' => 'required|string|max:255',
            'logo' => 'nullable|image',
            'copyright' => 'required|string|max:255',
            'promo' => 'nullable',
        ]);

        if ($request->file('logo') !== null) {
            $file = $request->file('logo');
            //Move Uploaded File
            $destinationPath = 'image';
            $fileName = 'logo.'.$file->getClientOriginalExtension();
            $file->move($destinationPath, $fileName);
            $information->logo = "$destinationPath/$fileName";
        }

        $information->name = $request['name'];
        $information->company = $request['company'];
        $information->inn = $request['inn'];
        $information->copyright = $request['copyright'];
        $information->promo = $request['promo'];
        $information->save();

        return redirect()->route('admin.information.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }
}
