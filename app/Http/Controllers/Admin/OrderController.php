<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('filter');
        $search = $request->input('search');
        if (isset($filter)) {
            $orders = Order::status($filter)->searchAdmin($search)->orderBy('created_at', 'desc')->paginate(10);
        } else {
            $orders = Order::searchAdmin($search)->orderBy('created_at', 'desc')->paginate(10);
        }
        return view('admin.orders.index', [
            'filter' => $filter,
            'orders' => $orders,
            'search' => $search,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $masters = User::where('role', 'master')->get();
        return view('admin.orders.create', [
            'order' => [],
            'masters' => $masters,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'number' => 'required|string|max:255|unique:orders',
            'master' => 'required',
        ]);

        Order::create($request->all());
        return redirect()->route('admin.order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        if (Gate::denies('seller')) {
            $masters = User::where('role', 'master')->get();
            return view('admin.orders.edit', [
                'order' => $order,
                'masters' => $masters,
            ]);
        }
        return redirect()->route('admin.order.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        if (Gate::denies('seller')) {
            $validator = $request->validate([
                'number' => [
                    'required',
                    'string',
                    'max:255',
                    \Illuminate\Validation\Rule::unique('orders')->ignore($order->id)],
                'master' => 'required',
            ]);

            $order->number = $request['number'];
            $order->status = $request['status'];
            if ($order->status == 'Заказ закрыт') {
                $order->closed_at = now();
            }
            $order->master = $request['master'];
            $order->notice = $request['notice'];
            $order->save();
        }
        return redirect()->route('admin.order.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        if (Gate::allows('admin')) {
            $order->delete();
        }
        return redirect()->route('admin.order.index');
    }
}
