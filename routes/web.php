<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('index');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth'] ], function () {
    Route::get('/', 'DashboardController@dashboard')->name('admin.index');
    Route::resource('/order', 'OrderController', ['as' => 'admin']);
    Route::group(['middleware' => ['can:admin'] ], function () {
        Route::resource('/user', 'UserController', ['as' => 'admin']);
        Route::resource('/office', 'OfficeController', ['as' => 'admin']);
        Route::resource('/information', 'InformationController', ['as' => 'admin']);
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
