<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Имя записи');
            $table->string('company')->comment('Название ИП');
            $table->string('inn')->comment('ИНН');
            $table->string('logo')->comment('Путь к логотипу');
            $table->string('copyright')->comment('Копирайт');
            $table->text('promo')->nullable()->comment('Рекламные объявления');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
