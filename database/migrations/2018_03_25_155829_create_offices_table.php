<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('Название офиса');
            $table->string('address')->comment('Адрес офиса');
            $table->string('phone')->nullable()->comment('Контактный телефон');
            $table->string('social')->nullable()->comment('Группа в социальной сети');
            $table->text('schedule')->comment('График работы');
            $table->text('map')->comment('Ссылка на онлайн-карту');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
