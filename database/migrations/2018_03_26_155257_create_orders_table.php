<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->unique()->comment('Номер заказа');
            $table->string('status')->comment('Статус заказа');
            $table->string('master')->comment('Мастер выполняющий заказ');
            $table->string('notice')->nullable()->comment('Примечание к заказу');
            $table->timestamp('closed_at')->nullable()->comment('Дата закрытия заказа');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
