<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->states('admin')->create();
        factory(App\User::class, 2)->states('master')->create();
        factory(App\User::class, 1)->states('seller')->create();
    }
}
