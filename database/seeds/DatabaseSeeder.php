<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OfficesTableSeeder::class);
        $this->call(OrdersTableSeeder::class);
        $this->call(InformationsTableSeeder::class);
    }
}
