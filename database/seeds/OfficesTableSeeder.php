<?php

use Illuminate\Database\Seeder;

class OfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Office::class, 1)->states('orshanka')->create();
        factory(App\Office::class, 1)->states('yurino')->create();
    }
}
