<?php

use Faker\Generator as Faker;

$factory->define(App\Information::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'company' => $faker->company,
        'inn' => \Faker\Factory::create('ru_RU')->inn,
        'logo' => $faker->url,
        'copyright' => $faker->text,
        'promo' => $faker->text,
    ];
});

$factory->state(App\Information::class, 'pronote', [
        'name' => 'pronote',
        'company' => 'ИП Столбов С.Ю.',
        'inn' => '121001258602',
        'logo' => 'image/logo.png',
        'copyright' => 'Оршанка 2018',
        'promo' => 'Здесь могла бы быть ваша реклама',
]);
