<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name('male'),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('123456'),
        'remember_token' => str_random(10),
    ];
});

/**
 * Состояние для учетной записи владельца (администратора)
 */
$factory->state(App\User::class, 'admin', [
      'email' => 'admin@test.ru',
      'role' => 'admin',
      'password' => bcrypt('123456'),
]);
/**
 * Состояние для учетной записи мастера по ремонту
 */
$factory->state(App\User::class, 'master', [
      'role' => 'master',
]);
/**
 * Состояние для учетной записи продавца консультанта
 */
$factory->state(App\User::class, 'seller', [
      'role' => 'seller',
]);
