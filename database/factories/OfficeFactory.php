<?php

use Faker\Generator as Faker;

$factory->define(App\Office::class, function (Faker $faker) {
    return [
        'name' => $faker->state,
        'address' => $faker->address,
        'phone' => $faker->e164PhoneNumber,
        'social' => $faker->url,
        'schedule' => $faker->text,
        'map' => $faker->url,
    ];
});

/**
 * Состояние для офиса в Оршанке
 */
$factory->state(App\Office::class, 'orshanka', [
    'name' => 'Оршанка',
    'address' => '425250, Республика Марий Эл, пгт. Оршанка, ул. Советская ул. д. 126, 2-й этаж',
    'phone' => '+7 902 744 98 97',
    'social' => 'https://vk.com/pronote_orshanka',
    'schedule' => '<ul><li>Понедельник – пятница с 8.30 до 17.00 <ul>
                        <li>обед с 12.00 до 13.00</li></ul></li>
                        <li>Суббота с 8.30 до 13.00</li></ul>',
    'map' => '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ad94a1ee82682287bf570abb9b8cf8027441a7706740e340962408b5e587f60de&amp;width=100%25&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>',
]);
/**
 * Состояние для офиса в Юрино
 */
$factory->state(App\Office::class, 'yurino', [
    'name' => 'Юрино',
    'address' => '425370, Республика Марий Эл, пгт. Юрино, Центральный проспект, д. 11а',
    'phone' => '+7 902 744 98 97',
    'social' => 'https://vk.com/pronote_yurino',
    'schedule' => '<ul><li>Понедельник – пятница с 8.30 до 17.00 <ul>
                        <li>обед с 12.00 до 13.00</li></ul></li>
                        <li>Суббота с 8.30 до 13.00</li></ul>',
    'map' => '<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A776131f36e146f16f7349b83ce4299dcd2159162375ede06b6eddba68e65d666&amp;width=100%25&amp;height=240&amp;lang=ru_RU&amp;scroll=true"></script>',
]);
