<?php

use Faker\Generator as Faker;

$factory->define(App\Order::class, function (Faker $faker) {
    $masters = data_get(\App\User::select('name')->where('role', 'master')->get(), '*.name');
    return [
        'number' => $faker->unique()->numberBetween($min = 10000, $max = 99999),
        'status' => $faker->randomElement($array = array('В ремонте', 'Требует согласования', 'Ремонт завершен')),
        'master' => $faker->randomElement($masters),
        'notice' => $faker->text($maxNbChars = 50),
    ];
});

/**
 * Состояние для завершенных ремонтов
 */
 $factory->state(App\Order::class, 'completed', [
     'status' => 'Заказ закрыт',
     'closed_at' => date("Y-m-d H:i:s"),
 ]);
