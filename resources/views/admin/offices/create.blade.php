@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header">Добавление офиса</h2>

      <div class="card-body">
        {{-- форма для создания офиса --}}
        <form class="form-horizontal" action="{{route('admin.office.store')}}" method="post">
          {{ csrf_field() }}

          {{-- Подключение формы --}}
          @include('admin.offices.partials.form')
        </form>

      </div>
    </div>
  </div>

@endsection
