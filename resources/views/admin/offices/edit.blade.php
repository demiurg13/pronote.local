@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header">Редактирование данных офиса</h2>

      <div class="card-body">
        {{-- форма для редактирования офиса --}}
        <form class="form-horizontal" action="{{route('admin.office.update', $office)}}" method="post">
          {{ method_field('put')}}
          {{ csrf_field() }}

          {{-- Подключение формы --}}
          @include('admin.offices.partials.form')
        </form>

      </div>
    </div>
  </div>

@endsection
