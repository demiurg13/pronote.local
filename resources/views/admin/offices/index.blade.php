@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header float-left">
        Список офисов
        <a href="{{route('admin.office.create')}}" class="btn btn-success float-right">
          Добавить офис
        </a>
      </h2>

        <div class="card-body">
          <table class="table table-responsiv table-striped table-bordered table-sm">
            <thead>
              <th scope="col">#</th>
              <th scope="col">Название</th>
              <th scope="col">Адрес</th>
              <th scope="col">Телефон</th>
              <th scope="col" class="text-right">Действия</th>
            </thead>
            <tbody>
              @forelse ($offices as $key => $office)
                <tr>
                  <th scope="row">{{++$key}}</th>
                  <td>{{$office->name}}</td>
                  <td>{{$office->address}}</td>
                  <td>{{$office->phone}}</td>
                  <td class="text-right">
                    <form onsubmit="if(confirm('Удалить?')){ return true }else{ return false }" action="{{route('admin.office.destroy', $office)}}" method="post">
                      {{ method_field('delete') }}
                      {{ csrf_field() }}

                      <a class="btn btn-outline-primary btn-sm" href="{{route('admin.office.edit', $office)}}">Edit</a>

                      <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                    </form>
                  </td>
                </tr>
              @empty
                <tr>
                  <td colspan="5" class="text-center"><h2>Данные отсутствуют</h2></td>
                </tr>
              @endforelse
            </tbody>
            <tfoot>
              <tr>
                <td colspan="5">
                  <ul class="pagination pull-right">
                    {{$offices->links()}}
                  </ul>
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
  </div>

@endsection
