@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group">
  <label for="name">Название офиса</label>
  <input type="text" class="form-control" name="name" id="name" placeholder="Введите название офиса" value="@if(old('name')){{old('name')}}@else{{$office->name or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="address">Адрес</label>
  <input type="text" class="form-control" name="address" id="address" placeholder="Введите адрес" value="@if(old('address')){{old('address')}}@else{{$office->address or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="phone">Телефон</label>
  <input type="phone" class="form-control" name="phone" id="phone" placeholder="Введите адрес" value="@if(old('phone')){{old('phone')}}@else{{$office->phone or ""}}@endif">
</div>

<div class="form-group">
  <label for="social">Группа в социальной сети</label>
  <input type="text" class="form-control" name="social" id="social" placeholder="Ссылка на группу в социальной сети" value="@if(old('social')){{old('social')}}@else{{$office->social or ""}}@endif">
</div>

<div class="form-group">
  <label for="schedule">Расписание</label>
  <textarea class="form-control" id="schedule" name="schedule">@if(old('schedule')){{old('schedule')}}@else{{$office->schedule or ""}}@endif</textarea>
</div>

<div class="form-group">
  <label for="map">Ссылка на онлайн-карту</label>
  <textarea class="form-control" name="map" id="map" rows="3" required>@if(old('map')){{old('map')}}@else{{$office->map or ""}}@endif</textarea>
</div>

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">
<a class="btn btn-outline-danger" href="{{ route('admin.office.index') }}">Отмена</a>
