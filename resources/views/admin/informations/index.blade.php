@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header float-left">
        Настройки приложения
        <a href="{{ route('admin.information.edit', $information) }}" class="btn btn-success float-right">
          Изменить
        </a>
      </h2>

      <div class="card-body">
        <img class="img-fluid" src="{{URL::asset($information->logo)}}" alt="logo">
        <p>{{$information->company}}<br>
        ИНН: {{$information->inn}}<br>
        &copy; {{$information->copyright}}</p>
      </div>
    </div>
    <br>
    <div class="card">
        <h3 class="card-header">Рекламное объявление</h3>
        <div class="card-body">
            {!! $information->promo !!}
        </div>
    </div>
   </div>

@endsection
