@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<input type="hidden" name="name" value="{{$information->name}}">

<div class="form-group">
  <label for="company">Название ИП</label>
  <input type="text" class="form-control" name="company" id="company" placeholder="Введите название" value="@if(old('company')){{old('company')}}@else{{$information->company or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="inn">ИНН</label>
  <input type="text" class="form-control" name="inn" id="inn" placeholder="Введите ИНН" value="@if(old('inn')){{old('inn')}}@else{{$information->inn or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="logo">Логотип</label>
  <input type="file" class="form-control" name="logo" id="logo" value="@if(old('logo')){{old('logo')}}@else{{$information->logo or ""}}@endif">
</div>

<div class="form-group">
  <label for="copyright">Копирайт</label>
  <input type="text" class="form-control" name="copyright" id="copyright" placeholder="Копирайт" value="@if(old('copyright')){{old('copyright')}}@else{{$information->copyright or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="promo">Рекламное объявление</label>
  <textarea class="form-control" name="promo" id="promo">@if(old('promo')){{old('promo')}}@else{{$information->promo or ""}}@endif</textarea>
</div>


<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">
<a class="btn btn-outline-danger" href="{{ route('admin.information.index') }}">Отмена</a>
