@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group">
  <label for="name">Имя</label>
  <input type="text" class="form-control" name="name" id="name" placeholder="Введите имя" value="@if(old('name')){{old('name')}}@else{{$user->name or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="email">Email</label>
  <input type="email" class="form-control" name="email" id="email" placeholder="Введите адрес электронной почты" value="@if(old('email')){{old('email')}}@else{{$user->email or ""}}@endif" required>
</div>

<div class="form-group">
  <label for="role">Роль пользователя</label>
  <select class="custom-select" name="role" id="role">
    @if (isset($user->id))
      {{--<option value="user" @if ($user->role == 'user') selected="" @endif>Пользователь</option>--}}
      <option value="seller" @if ($user->role == 'seller') selected="" @endif>Продавец-консультант</option>
      <option value="master" @if ($user->role == 'master') selected="" @endif>Мастер по ремонту</option>
      <option value="admin" @if ($user->role == 'admin') selected="" @endif>Владелец (администратор)</option>
    @else
      {{--<option value="user">Пользователь</option>--}}
      <option value="seller">Продавец-консультант</option>
      <option value="master">Мастер по ремонту</option>
      <option value="admin">Владелец (администратор)</option>
    @endif
  </select>
</div>

<div class="form-group">
  <label for="password">Пароль</label>
  <input type="password" class="form-control" name="password" id="password">
</div>

<div class="form-group">
  <label for="password_confirmation">Подтверждение</label>
  <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
</div>

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">
<a class="btn btn-outline-danger" href="{{ route('admin.user.index') }}">Отмена</a>
