@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header">Редактирование заказа</h2>

      <div class="card-body">
        {{-- форма для редактирования пользователя --}}
        <form class="form-horizontal" action="{{route('admin.order.update', $order)}}" method="post">
          {{ method_field('put')}}
          {{ csrf_field() }}

          {{-- Подключение формы --}}
          @include('admin.orders.partials.form')
        </form>

      </div>
    </div>
  </div>

@endsection
