@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

        <h2 class="card-header float-left">
          Список заказов
          <a href="{{route('admin.order.create')}}" class="btn btn-success float-right">
            Добавить заказ
          </a>
        </h2>

        <div class="card-body">
            <form class="form-control-sm" action="{{route('admin.order.index')}}" method="get">
                <div class="form-row">
                    <div class="col">
                        <input class="form-control" type="search" name="search" placeholder="{{$search or "Поиск"}}" aria-label="Search">
                    </div>

                    <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Найти</button>
                </div>
            </form>
            <hr>
            <!-- Nav pills -->
            <ul class="nav nav-tabs nav-fill">
              <li class="nav-item">
                <a class="nav-link @if ($filter == '') active @endif" href="{{ route('admin.order.index') }}">Все заказы</a>
              </li>
              <li class="nav-item">
                <a class="nav-link @if ($filter == 'В ремонте') active @endif" href="{{ route('admin.order.index', ['filter'=>'В ремонте']) }}">В ремонте</a>
              </li>
              <li class="nav-item">
                <a class="nav-link @if ($filter == 'Требует согласования') active @endif" href="{{ route('admin.order.index', ['filter'=>'Требует согласования']) }}">Требует согласования</a>
              </li>
              <li class="nav-item">
                <a class="nav-link @if ($filter == 'Ремонт завершен') active @endif" href="{{ route('admin.order.index', ['filter'=>'Ремонт завершен']) }}">Ремонт завершен</a>
              </li>
              <li class="nav-item">
                <a class="nav-link @if ($filter == 'Заказ закрыт') active @endif" href="{{ route('admin.order.index', ['filter'=>'Заказ закрыт']) }}">Завершеные заказы</a>
              </li>
            </ul>
            <table class="table table-responsiv table-striped table-bordered table-sm">
              <thead>
                <th scope="col">Номер заказа</th>
                <th scope="col">Статус</th>
                <th scope="col">Мастер</th>
                <th scope="col" class="text-right">Действия</th>
              </thead>
              <tbody>
                @forelse ($orders as $key => $order)
                  <tr>
                    <td>{{$order->number}}</td>
                    <td>{{$order->status}}</td>
                    <td>{{$order->master}}</td>
                    <td class="text-right">

                    @can ('admin')
                      <form onsubmit="if(confirm('Удалить?')){ return true }else{ return false }" action="{{route('admin.order.destroy', $order)}}" method="post">
                        {{ method_field('delete') }}
                        {{ csrf_field() }}
                    @endcan

                    @cannot ('seller')
                        <a class="btn btn-outline-primary btn-sm" href="{{route('admin.order.edit', $order)}}">Edit</a>
                    @endcan

                    @can ('admin')
                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                      </form>
                    @endcan
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5" class="text-center"><h2>Данные отсутствуют</h2></td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="5">
                    <ul class="pagination pull-right">
                        @if (isset($search))
                            {{$orders->appends(['search' => $search])->links()}}
                        @else
                            {{$orders->appends(['filter' => $filter])->links()}}
                        @endif

                    </ul>
                  </td>
                </tr>
              </tfoot>
            </table>

        </div>
      </div>
  </div>

@endsection
