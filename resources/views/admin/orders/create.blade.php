@extends('admin.layouts.app_admin')

@section('content')

  <div class="container">
    <div class="card">

      <h2 class="card-header">Добавление заказа</h2>

      <div class="card-body">
        {{-- форма для создания пользователя --}}
        <form class="form-horizontal" action="{{route('admin.order.store')}}" method="post">
          {{ csrf_field() }}

          {{-- Подключение формы --}}
          @include('admin.orders.partials.form')
        </form>

      </div>
    </div>
  </div>

@endsection
