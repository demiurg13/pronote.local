@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

<div class="form-group">
  <label for="number">Номер заказа</label>
  <input type="text" class="form-control" name="number" id="number" placeholder="Введите номер заказа из базы данных" value="@if(old('number')){{old('number')}}@else{{$order->number or ""}}@endif" @isset ($order->closed_at) disabled @endisset required>
</div>

<div class="form-group">
  <label for="status">Статус заказа</label>
  <select class="custom-select" name="status" id="status" @isset ($order->closed_at) disabled @endisset>
    @if (isset($order->id))
      <option @if ($order->status == 'В ремонте') selected="" @endif>В ремонте</option>
      <option @if ($order->status == 'Требует согласования') selected="" @endif>Требует согласования</option>
      <option @if ($order->status == 'Ремонт завершен') selected="" @endif>Ремонт завершен</option>
      <option @if ($order->status == 'Заказ закрыт') selected="" @endif>
          Заказ закрыт @if ($order->closed_at) {{ $order->closed_at }} @endif
      </option>
    @else
      <option>В ремонте</option>
      <option>Требует согласования</option>
      <option>Ремонт завершен</option>
    @endif
  </select>
</div>

<div class="form-group">
  <label for="master">Мастер выполняющий заказ</label>
  <select class="custom-select" name="master" id="master" @isset ($order->closed_at) disabled @endisset>
    @if (isset($order->id))
        @forelse ($masters as $master)
            <option value="{{ $master->name }}" @if ($order->master == $master->name) selected="" @endif>{{ $master->name }}</option>
          @empty
            <option value="{{ $order->master }}" selected="">{{ $order->master }}</option>
          @endforelse
    @else
        @forelse ($masters as $master)
            <option value="{{ $master->name }}" >{{ $master->name }}</option>
        @empty
            <option selected="">Необходимо добавить пользователя с ролью мастера по ремонту</option>
        @endforelse
    @endif
  </select>
</div>

<div class="form-group">
  <label for="notice">Примечание к заказу</label>
  <input type="text" class="form-control" name="notice" id="notice" placeholder="Введите примечание к заказу" value="@if(old('notice')){{old('notice')}}@else{{$order->notice or ""}}@endif" @isset ($order->closed_at) disabled @endisset>
</div>

<hr />

<input class="btn btn-primary" type="submit" value="Сохранить">
<a class="btn btn-outline-danger" href="{{ route('admin.order.index') }}">Отмена</a>
