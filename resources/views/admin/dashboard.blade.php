@extends('admin.layouts.app_admin')

@section('content')

    <div class="container">
        <div class="card-deck">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Заказы ожидающие выдачи</h5>
              <hr>
              <ul>
                  @forelse ($completedOrders as $order)
                      <li><a href="{{route('admin.order.edit', $order)}}"><kbd>{{ $order->number }}</kbd></a> - {{ $order->notice }}</li>
                  @empty
                      <p>Нет заказов</p>
                  @endforelse
              </ul>
            </div>
          </div>
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Заказы ожидающие согласования</h5>
              <hr>
              <ul>
                  @forelse ($waitOrders as $order)
                      <li><a href="{{route('admin.order.edit', $order)}}"><kbd>{{ $order->number }}</kbd></a> - {{ $order->notice }}</li>
                  @empty
                      <p>Нет заказов</p>
                  @endforelse
              </ul>
            </div>
          </div>
        </div>
    </div>

@endsection
