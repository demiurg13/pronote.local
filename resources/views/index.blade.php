
<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&amp;subset=cyrillic-ext" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">

  <body>

    <header>
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{URL::asset($information->logo)}}" height="35" alt="logo">
                    <strong>{{ config('app.name', 'Laravel') }}</strong>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                          <li class="nav-item">
                            <a class="nav-link disabled" href="#"><kbd>Устали без любимого девайса? Не ждите звонка! </kbd></a>
                          </li>
                        <form class="form-inline" action="{{route('index')}}" method="get">
                            <div class="input-group mr-sm-2 ">
                              <input class="form-control" type="search" name="search" placeholder="{{$search or "Введите номер заказа"}}" aria-label="Search">
                              <div class="input-group-append">
                                <button class="btn btn-outline-secondary my-2 my-sm-0" type="submit">Найти</button>
                              </div>
                            </div>
                        </form>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main class="py-4" role="main">
        <div class="container">
            <!-- Поиск -->
            @isset($order)
                <div class="alert alert-secondary alert-dismissible col-8 offset-2 fade show" role="alert">
                    <h4 class="alert-heading"><strong>Заявка № {{ $order->number }}</strong></h4>
                    <hr>
                    <p>
                        <strong>Статус заявки:</strong> {{ $order->status }}<br>
                        <strong>Мастер выполняющий заказ:</strong> {{ $order->master }}<br>
                        @isset($order->notice)
                            <strong>Примечание:</strong> {{ $order->notice }}
                        @endisset
                    </p>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endisset
            <!-- Рекламный блок -->
            @isset ($information->promo)
                <div class="card">
                  <div class="card-body">
                    {!! $information->promo !!}
                  </div>
                </div>
                <br>
            @endisset

            <!-- Офисы -->
            <div class="card">
                <div class="card-body">
                  <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                      @forelse ($offices as $key => $office)
                          <li class="nav-item">
                            <a class="nav-link @if ($loop->first) active @endif" id="pills-o{{$key}}-tab" data-toggle="pill" href="#pills-o{{$key}}" role="tab" aria-controls="pills-o{{$key}}" aria-selected="@if ($loop->first) true @else false @endif">{{$office->name}}</a>
                          </li>
                      @empty

                      @endforelse
                  </ul>

                  <div class="tab-content" id="pills-tabContent">
                      @forelse ($offices as $key => $office)
                          <div class="tab-pane fade @if ($loop->first) show active @endif" id="pills-o{{$key}}" role="tabpanel" aria-labelledby="pills-o{{$key}}-tab">

                              <div class="card-deck">
                                  <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">
                                            <strong>Мы работаем</strong>
                                        </h5>
                                        <hr>
                                      <p class="card-text">{!!$office->schedule!!}</p>
                                    </div>
                                  </div>
                                  <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">
                                            <strong>Наш адрес</strong>
                                        </h5>
                                        <hr>
                                      <p class="card-text">{!!$office->address!!}</p>
                                    </div>
                                  </div>
                                  <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">
                                            <strong>Связаться с нами</strong>
                                        </h5>
                                        <hr>
                                      <p class="card-text">Телефон: {{$office->phone}}</p>
                                      <p class="card-text"><a href="{{$office->social}}">{{$office->social}}</a></p>
                                    </div>
                                  </div>
                                </div>
                                <hr>
                                <div class="card">
                                    {!! $office->map !!}
                                </div>
                          </div>
                      @empty

                      @endforelse
                  </div>
              </div>
            </div>

        </div>
    </main>

    <footer class="footer">
        <div class="container">
            <span class="text-muted">&copy; {{ $information->copyright}} </span>
            <span class="float-right"><strong>{{$information->company}}</strong> ИНН: {{$information->inn}} </span>
        </div>
    </footer>

  </body>
</html>
